var ids = {1 : "Belarus Space Agency", 2 : "Aeronautics and Space Research and Diffusion Center", 3 : "Mexican Space Agency", 4 : "Algerian Space Agency", 5 : "Asia Pacific Multilateral Cooperation in Space Technology and Applications", 6 : "Asia-Pacific Regional Space Agency Forum", 7 : "Asia-Pacific Space Cooperation Organization", 8 : "Austrian Space Agency", 9 : "Azerbaijan National Aerospace Agency", 10 : "Belgian Institute for Space Aeronomy", 11 : "Bolivarian Agency for Space Activities", 12 : "Brazilian Space Agency", 13 : "UK Space Agency", 14 : "Bolivian Space Agency", 15 : "Bulgarian Space Agency", 16 : "Canadian Space Agency", 17 : "China National Space Administration", 18 : "Colombian Space Commission", 19 : "Centre for Remote Imaging, Sensing and Processing", 20 : "Commonwealth Scientific and Industrial Research Organisation", 21 : "Consultative Committee for Space Data Systems", 22 : "Committee on Space Research", 23 : "Croatian Space Agency", 24 : "Ministry of Transport of the Czech Republic - Space Technologies and Satellite Systems Department", 25 : "Danish National Space Center", 26 : "Technical University of Denmark - National Space Institute", 27 : "European Space Agency", 28 : "Geo-Informatics and Space Technology Development Agency", 29 : "German Aerospace Center", 30 : "Hungarian Space Office", 31 : "Indian Space Research Organization", 32 : "Institute for Space Applications and Remote Sensing", 33 : "Instituto Nacional de Técnica Aeroespacial", 34 : "Iranian Space Agency", 35 : "Israeli Space Agency", 36 : "Italian Space Agency", 37 : "Japan Aerospace Exploration Agency", 38 : "National Space Agency (KazCosmos)", 39 : "Kazakh Space Research Institute", 40 : "Korean Committee of Space Technology", 41 : "Korea Aerospace Research Institute", 42 : "Lithuanian Space Association", 43 : "Malaysian National Space Agency", 44 : "National Aeronautics and Space Administration", 45 : "National Authority for Remote Sensing and Space Sciences", 46 : "National Center of Space Research", 47 : "National Commission for Aerospace Research and Development", 48 : "National Commission for Space Research", 49 : "National Space Activities Commission", 50 : "National Institute of Aeronautics and Space", 51 : "National Remote Sensing Center of Mongolia", 52 : "National Remote Sensing Center of Tunisia", 53 : "Uzbek State Space Research Agency (UzbekCosmos)", 54 : "National Space Agency of Ukraine", 55 : "National Space Organization", 56 : "National Space Research and Development Agency", 57 : "Netherlands Institute for Space Research", 58 : "Norwegian Space Centre", 59 : "Pakistan Space and Upper Atmosphere Research Commission", 60 : "FCT Space Office", 61 : "Romanian Space Agency", 62 : "Royal Centre for Remote Sensing", 63 : "Russian Federal Space Agency (ROSCOSMOS)", 64 : "Sri Lanka Space Agency", 65 : "TUBITAK Space Technologies Research Institute", 66 : "Soviet Space Program", 67 : "Space Research and Remote Sensing Organization", 68 : "Space Research Centre", 69 : "South African National Space Agency", 70 : "Space Research Institute of Saudi Arabia", 71 : "Swiss Space Office", 72 : "Turkmenistan National Space Agency", 73 : "United Nations Office for Outer Space Affairs", 225 : "1worldspace", 75 : "United Nations Committee on the Peaceful Uses of Outer Space", 76 : "Swedish National Space Board", 77 : "OHB System", 78 : "Thales Alenia Space", 79 : "JSC Information Satellite Systems", 80 : "Boeing", 81 : "Astrium Satellites", 82 : "Lockheed Martin", 83 : "Space Systems/Loral", 84 : "Amsat", 85 : "Astronautic Technology Sdn Bhd", 86 : "Ball Aerospace & Technologies Corp.", 87 : "British Aerospace", 88 : "China Aerospace Science and Technology Corporation", 89 : "Fairchild Space and Electronics Division", 90 : "Fokker Space & Systems", 91 : "General Electric", 92 : "Hawker Siddeley Dynamics", 93 : "Hughes Aircraft", 94 : "IHI Corporation", 95 : "Israel Aerospace Industries", 96 : "Khrunichev State Research and Production Space Center", 97 : "NPO Lavochkin", 98 : "Mitsubishi Heavy Industries", 99 : "Northrop Grumman Space Technology", 100 : "Orbital Sciences Corporation", 101 : "Philco Ford", 102 : "Rockwell International", 103 : "RKK Energiya", 104 : "SPAR Aerospace", 105 : "SpaceDev", 106 : "General Dynamics", 107 : "Surrey Satellite Technology Ltd", 108 : "Swales Aerospace", 109 : "Turkish Aerospace Industries", 110 : "TRW", 111 : "Progress State Research and Production Rocket Space Center", 112 : "Yuzhnoye Design Bureau", 113 : "INVAP", 184 : "China Aerospace Science and Industry Corporation", 115 : "Arianespace", 116 : "EADS Astrium Space Transportation", 117 : "Eurockot Launch Services", 118 : "International Launch Services", 119 : "ISC Kosmotras", 120 : "SpaceQuest, Ltd.", 121 : "SpaceX", 122 : "Sea Launch", 123 : "Starsem SA", 124 : "United Launch Alliance", 125 : "A.M. Makarov Yuzhny Machine-Building Plant", 126 : "Deep Space Industries", 127 : "Robotics Institute", 128 : "Planetary Resources", 129 : "Tethers Unlimited, Inc.", 130 : "RUAG Space", 131 : "Andrews Space", 132 : "Kongsberg Defence & Aerospace", 133 : "Aerojet", 134 : "American Rocket Company", 135 : "Rocketdyne", 136 : "Ad Astra Rocket Company", 137 : "Reaction Engines Ltd.", 138 : "Snecma", 139 : "Armadillo Aerospace", 140 : "Bigelow Aerospace", 141 : "Blue Origin", 142 : "Copenhagen Suborbitals", 143 : "PlanetSpace", 144 : "Scaled Composites", 145 : "XCOR  Aerospace", 146 : "Canadian Arrow", 147 : "Rocket Lab Ltd", 148 : "Scorpius Space Launch Company", 149 : "Interorbital Systems", 150 : "Masten Space Systems", 151 : "Swedish Space Corp", 152 : "UP Aerospace", 153 : "McDonnell Douglas", 154 : "Production Corporation Polyot", 156 : "Alliant Techsystems", 157 : "Bristol Aerospace Company", 158 : "Chrysler", 159 : "Avio S.p.A", 160 : "Royal Australian Air Force", 161 : "United States Air Force", 162 : "People\\\'s Liberation Army", 163 : "Russian Aerospace Defence Forces", 165 : "US Army", 166 : "US Navy", 167 : "Space Florida", 175 : "Ministry of Defence of the Russian Federation", 177 : "China Great Wall Industry Corporation", 178 : "Airbus Defence and Space", 179 : "Orbital ATK", 181 : "National Reconnaissance Office", 182 : "National Space Agency of the Republic of Kazakhstan", 183 : "Unknown", 186 : "Polish Space Agency", 187 : "GK Launch Services", 188 : "Gilmour Space Technologies", 273 : "Gazprom Space Systems", 190 : "Antrix Corporation Limited", 191 : "United Space Alliance", 192 : "Lockheed Space Operations Company", 193 : "Russian Space Forces", 194 : "ExPace", 195 : "Sandia National Laboratories", 196 : "Land Launch", 197 : "Lockheed Martin Space Operations", 198 : "Mohammed bin Rashid Space Centre", 199 : "Virgin Orbit", 201 : "Vector", 202 : "Iridium Communications", 203 : "SES", 204 : "Globalstar", 205 : "Inmarsat", 206 : "Intelsat", 207 : "Arab Satellite Communications Organization", 208 : "Telesat", 224 : "GeoOptics", 226 : "PanAmSat", 210 : "National Oceanic and Atmospheric Administration", 211 : "National Security Agency", 212 : "Digital Globe", 213 : "Missile Defense Agency", 214 : "Spire Global", 227 : "UK Ministry Of Defence", 228 : "National Space Development Agency of Japan", 229 : "Eutelsat", 230 : "Broadcasting Satellite System Corporation", 231 : "SKY Perfect JSAT Group", 232 : "European Organisation for the Exploitation of Meteorological Satellites", 233 : "Direction générale de l\\\'armement", 234 : "XTAR LLC", 235 : "Thaicom", 236 : "DirecTV", 237 : "PT Telkom", 238 : "Hisdesat", 239 : "Satmex", 240 : "Optus", 241 : "WildBlue", 242 : "Paradigm Secure Communications", 243 : "Hughes", 244 : "Star One", 245 : "Regional African Satellite Communication Organization", 246 : "Vietnam Posts and Telecommunications Group", 247 : "Türksat", 248 : "ProtoStar", 249 : "Echostar", 250 : "HispaSat", 251 : "AlYahSat", 252 : "MEASAT Satellite Systems", 253 : "French Armed Forces", 254 : "British Satellite Broadcasting", 255 : "Avanti Communications", 256 : "Mexican Satellite System", 257 : "Northrop Grumman Innovation Systems", 259 : "LandSpace", 260 : "Planet", 261 : "Korean Astronaut Program", 265 : "Firefly Aerospace", 272 : "Chinarocket Co., Ltd.", 263 : "OneSpace", 264 : "Vietnam National Space Center", 266 : "Relativity Space", 267 : "PT Pasifik Satelit Nusantara", 268 : "SpaceIL", 269 : "Defence Research and Development Organisation", 270 : "Strategic Missile Troops", 271 : "Army Ballistic Missile Agency", 274 : "iSpace", 275 : "Republic of Korea Army", 276 : "KT Corporation", 278 : "ICEYE", 279 : "BlackSky", 280 : "United Arab Emirates Armed Forces", 282 : "Aevum", 283 : "Intuitive Machines", 285 : "Astra Space"};
'use strict';


var express = require('express'),
    bodyParser = require('body-parser'),
    app = express(),
    port = 5000;
const loadJsonFile = require('load-json-file');

const https = require('https');
const date = require('date-and-time');
var mydate = require('current-date');
var Promise = require('promise');

global.response = '';

app.use(bodyParser.json());


const getContent = function(url, ids, req, res) {
    return new Promise((resolve, reject) => {
      const lib = url.startsWith('https') ? require('https') : require('http');
      const request = lib.get(url, (response) => {
        // handle http errors
        if (response.statusCode < 200 || response.statusCode > 299) {
           reject(new Error('Failed to load page, status code: ' + response.statusCode));
        }
        const body = [];
        response.on('data', (chunk) => body.push(chunk));
        response.on('end', () => {
          var bodyStr = '';
          var decoder = new TextDecoder("utf-8")
          for (let i of body) {
            bodyStr += decoder.decode(i);
          }
          var responseData = JSON.parse(bodyStr);
          var launch = responseData.launches[0];

          res.json(getLaunchCard(launch))    
        
        // res.json({
        //         fulfillmentText : out+"\n"+out2,
        //         // responseId : responseId
        //     });
          
        });
      });
      request.on('error', (err) => reject(err));
    });
}

app.post('/', function (req, res) {
    var body = req.body;
    var session = body.session
    var responseId = body.responseId
    var intentName = body.queryResult.intent.displayName;
    var exampleResponse = global.response;

    // console.log(global.response);
    res.body = global.response;

    console.log(intentName)
    if (intentName == 'Default Welcome Intent') {

        res.json({
            // fulfillmentText : 'Welcome to Rocket Tracker, you can ask when the next rocket launch is, and specify an agency, location, or launch vehicle if you want to.',
        
            
                payload: {
                  google: {
                    expectUserResponse: true,
                    richResponse: {
                      items: [
                        {
                          simpleResponse: {
                            textToSpeech : "Welcome to Rocket Tracker"
                          }
                        },
                        {
                          basicCard: {
                            title: "Rocket Tracker",
                            subtitle: "Simple & useful info about upcoming rocket launches",
                            formattedText: "Welcome!  \nTo see the next rocket launch, tap the \"Next Rocket Launch\" button on the bottom of your screen.",
                            image: {
                              url: "https://cms.qz.com/wp-content/uploads/2018/03/spacex-falcon-heavy-space-internet-satellite-elon-musk.jpg",
                              accessibilityText: "Rocket Tracker Home"
                            },
                            // buttons: [
                            //   {
                            //     title : "This is a button",
                            //     openUrlAction : {
                            //       url: "https://assistant.google.com/"
                            //     }
                            //   }
                            // ],
                            imageDisplayOptions: "CROPPED"
                          }
                        }
                      ], 
                      suggestions: [
                          {
                              title : 'Next Rocket Launch'
                          },
                        //   {
                        //       title : 'Suggestion 2'
                        //   }
                      ]
                    }
                  }
                }
              
            

        });
    } else if (intentName == 'launches') {
        // Deprecated
        var responseText = getContent('http://launchlibrary.net/1.4/launch?mode=verbose&next=1&offset=0', ids, req, res);
    } else if (intentName == 'launchesOffset') {
        global.offset = req.body.queryResult.parameters.offset - 1;
        var responseText = getContent(`http://launchlibrary.net/1.4/launch?mode=verbose&next=1&offset=${offset}`, ids, req, res);
    }


    console.log();
});

var server = app.listen(port, function () {

    var host = server.address().address
    var port = server.address().port

    console.log('Example app listening at http://%s:%s', host, port)

});

const getLaunchCard = function(launch, offset) {
    global.offset = (typeof(global.offset) == 'undefined') ? 0 : global.offset;
    var time = launch.net;
    var launchTime = date.parse(time, 'MMMM D, YYYY HH:mm:ss UTC', true);
    var currentTime = date.parse(mydate(), "YYYY-MM-DD HH:mm:ss");
    var difference = date.subtract(launchTime, currentTime);

    var days = difference.toDays();
    var hours = difference.toHours() - 24*days;
    var min = difference.toMinutes() - 60*difference.toHours(); 
    var sec = difference.toSeconds() - 60*difference.toMinutes();
    var nameData = launch.name.split("|");
    var launchVehicle = nameData[0].substring(0, nameData[0].length-1);
    var payload = nameData[1].substring(1);
    var lsp = ids[parseInt(launch.lsp)];
    var rocketName = launch.rocket.name;

    let day_s = (days!=1) ? 's' : '';
    let hour_s = (hours!=1) ? 's' : '';
    let minute_s = (min!=1) ? 's' : '';
    let sec_s = (sec!=1) ? 's' : '';
    var timeStr = (days > 0) ? `${days} day${day_s}, ${hours} hour${hour_s}, ${min} minute${minute_s}` : (hours > 0) ? `${hours} hour${hour_s}, ${min} minute${minute_s}` : `${min} minute${minute_s}, ${sec} second${sec_s}`;
    var response = {payload: {
        google: {
          expectUserResponse: true,
          richResponse: {
            items: [
              {
                simpleResponse: {
                  textToSpeech : `Here's some info on the ${global.offset+1}${numberSuffix(global.offset+1)} upcoming launch:`
                }
              },
              {
                basicCard: {
                  title: `${payload} | ${launchVehicle}`,
                  subtitle: `Launches in ${timeStr}`,
                  formattedText : ``,
                  image: {
                    url: launch.rocket.imageURL,
                    accessibilityText: rocketName
                  },
                //   buttons: [],
                  imageDisplayOptions: "CROPPED"
                // imageDisplayOptions : "DEFAULT"
                }
              }
            ], 
            suggestions: []
          }
        }
      }
    }

    if (launch.vidURLs.length > 0) {
        response.payload.google.richResponse.items[1].basicCard.buttons = [
            {
                title : "Livestream",
                openUrlAction : {
                  url: launch.vidURLs[0]
                }
              }
        ]
    }
    var rocketImages = {
        "Astra Rocket 3.0" : "https://www.nextbigfuture.com/wp-content/uploads/2020/02/astra1-730x430.jpeg",
        "Falcon 9 Block 5" : "https://mk0spaceflightnoa02a.kinstacdn.com/wp-content/uploads/2018/09/DmroWXeU4AAcxaP.jpg",
        "Long March 3B" : "https://www.nasaspaceflight.com/wp-content/uploads/2019/01/201910-211300.jpg",
        "Soyuz 2.1b/Fregat-M" : "https://everydayastronaut.com/wp-content/uploads/2018/10/Soyuz-2-launch.jpg",
        "Vega" : "https://images.firstpost.com/optimize/XQmwNAwaPKTUc8Dq14RiPR9gkDo=/images.firstpost.com/wp-content/uploads/2019/07/esa-vega-rocket.jpg",
        "GSLV Mk II" : "https://www.thehindu.com/news/national/article18596139.ece/ALTERNATES/LANDSCAPE_1200/29THGSLVNEW",
        
    }
    // Random tweak
    if (payload.substring(0, 4) == 'SpX ') {
        payload = payload.substring(4);
        response.payload.google.richResponse.items[1].basicCard.title = `${payload} | ${launchVehicle}`;
    }
    

    if (launch.rocket.imageURL.indexOf('placeholder') != -1 && rocketName in rocketImages) {
        response.payload.google.richResponse.items[1].basicCard.image.url = rocketImages[rocketName];
    };

    response.payload.google.richResponse.items[1].basicCard.formattedText += `Scheduled for ${time}  \n`
    if (launch.missions.length != 0) {
        var description = launch.missions[0].description;
        response.payload.google.richResponse.items[1].basicCard.formattedText += description;
    }

    if (global.offset == 0) {
        response.payload.google.richResponse.suggestions = [
        {
            title : `${global.offset+2}${numberSuffix(global.offset+2)} upcoming launch`
        }];
    } else {
        response.payload.google.richResponse.suggestions = [
            {
                title : `${global.offset}${numberSuffix(global.offset)} upcoming launch`
            },
            {
                title : `${global.offset+2}${numberSuffix(global.offset+2)} upcoming launch`
            }
            ];
    };
    response.payload.google.richResponse.suggestions.push({title:'Home'});

    return response
}

const numberSuffix = function(num) {
    return (num == 1) ? 'st' : (num == 2) ? 'nd' : (num == 3) ? 'rd' : 'th';
}