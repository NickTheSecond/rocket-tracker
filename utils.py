import json, os, sys, time, codecs, logging
import requests
import datetime
import dateutil
from dateutil.parser import parse

def getData(prefix, name, params):
    startTime = time.time()
    url = 'https://launchlibrary.net/1.4/{}'.format(prefix)
    r = requests.get(url = url, params = params) 
    data = r.json() 
    with open(os.path.join('data','{}.json'.format(name)), 'w') as f:
        f.write(json.dumps(data, indent=4, sort_keys=True))
    logging.debug('Got data for {} ({:.2f}s)'.format(name, time.time()-startTime))

def loadData(path, data):
    startTime = time.time()
    files = []
    for filename in os.listdir(path):
        filename = os.path.join(path, filename)
        if os.path.isfile(filename) and filename[-5:] == '.json':
            files.append(filename)
    for filename in files:
        name = os.path.basename(filename)[:-5]
        if not os.path.exists(filename):
            continue
        with open(filename, 'r') as f:
            fileJson = json.loads(f.read())
            data[name] = fileJson
            print('',end='')
    logging.debug('Done loading ({:.2f}s)'.format(time.time()-startTime))

from threading import Event, Thread

def call_repeatedly(interval, func, *args):
    stopped = Event()
    def loop():
        while not stopped.wait(interval): # the first call is in `interval` secs
            func(*args)
    Thread(target=loop).start()    
    return stopped.set

def reloadData(data):
    logging.info("Reloading data")
    dataNew = {}
    # Getting new data
    getData('launch','launch',{'mode':'verbose','next':10**6})
    getData('launch','launchAll',{'mode':'verbose','limit':10**6})
    prefixes = ['agency','agencytype','launchstatus','location','mission','missiontype','pad','payload','rocket','rocketfamily']
    for prefix in prefixes:
        getData(prefix, prefix, {'mode':'verbose','limit':10**6})
    loadData('data', dataNew)
    data = dataNew
    return 1


def timeDeltaExpand(t):
    seconds = int(t.total_seconds())
    minutes, seconds = divmod(seconds, 60)
    hours, minutes = divmod(minutes, 60)
    days, hours = divmod(hours, 24)

    # return {'days':days,'hours':hours,'minutes':minutes,'seconds':seconds}
    return days, hours, minutes, seconds

def formatCountdown(d, h, m, s):
    result = ''
    plurals = ['s','s','s','s']
    if d == 1:
        plurals[0] = ''
    if h == 1:
        plurals[1] = ''
    if m == 1:
        plurals[2] = ''
    if s == 1:
        plurals[3] = ''
    if d != 0:
        # >24h until launch
        result = '{} day{}, {} hour{}, {} minute{}'.format(d, plurals[0], h, plurals[1], m, plurals[2])
    elif h != 0:
        # 1-23h until launch
        result = '{} hour{}, {} minute{}'.format(h, plurals[1], m, plurals[2])
    else:
        # >1h until launch, show seconds
        result = '{} minute{}, {} second{}'.format(m, plurals[2], s, plurals[3])
    return result

def formatNumberSuffix(n):
    result = str(n)
    suffixes = {1:'st', 2:'nd', 3:'rd'}
    if n % 10 in suffixes:
        result += suffixes[n % 10]
    else:
        result += 'th'
    return result