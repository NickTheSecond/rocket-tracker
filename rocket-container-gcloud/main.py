import json, os, sys, time, codecs, logging
import requests
import datetime
import dateutil
from dateutil.parser import parse
from flask import Flask, request, abort, render_template
from utils import *
import threading

# Google cloud stuff
os.environ["GCLOUD_PROJECT"] = 'rocket-2db63'
from google.cloud import datastore
datastore_client = datastore.Client()

os.chdir(os.path.dirname(os.path.abspath(__file__)))

app = Flask(__name__)
logging.basicConfig(level=logging.INFO)
data = {}
userData = {}

def store_time(dt):
    entity = datastore.Entity(key=datastore_client.key('visit'))
    entity.update({
        'timestamp': dt
    })
    datastore_client.put(entity)

def fetch_times(limit):
    query = datastore_client.query(kind='visit')
    query.order = ['-timestamp']
    times = query.fetch(limit=limit)
    return times

def getStuff():
    r = requests.get(url = 'http://launchlibrary.net/1.4/launch?mode=verbose&next=100000')
    rJson = r.json()
    # logging.info(rJson)
    
    query = datastore_client.query(kind='launch')
    oldData = query.fetch()
    for entity in oldData:
        datastore_client.delete(entity.key)
    
    for i, launch in enumerate(rJson['launches']):
        launch['closest'] = i

        entity = datastore.Entity(key=datastore_client.key('launch'))
        entity.update(launch)
        try:
            datastore_client.put(entity)
        except:
            logging.info('Launch {}: Too long'.format(i))

    logging.info("Saved launchData to datastore")

    
def loadData(data):
    query = datastore_client.query(kind='launch')
    query.order = ['closest']
    result = query.fetch()
    launches = []
    for i, entity in enumerate(result):
        launch = dict(entity)
        launches.append(launch)


getStuff()
loadData(data)

def reloadData(delay, task, data):
    next_time = time.time() + delay
    while True:
        time.sleep(max(0, next_time - time.time()))
        task(data)
        print('Reloaded data')
        next_time += (time.time() - next_time) // delay * delay + delay

threading.Thread(target=lambda: reloadData(5, loadData, data)).start()


# @app.route('/', methods=['GET'])
# def root():
#     store_time(datetime.datetime.now())

#     # Fetch the most recent 10 access times from Datastore.
#     times = fetch_times(10)

#     return render_template(
#         'index.html', times=times)

@app.route('/', methods=['POST'])
def webhook():
    global userData
    global data
    if request.method == 'POST':
        logging.debug(request.json)
        requestData = request.json
        intent = requestData['queryResult']['intent']['displayName']
        session = requestData['session']
        
        if session not in userData:
            userData[session] = {'offset' : 0}
        
        # Handling button press
        if 'button' in requestData['queryResult']['parameters']:
            button = requestData['queryResult']['parameters']['button']
            if button == 'Next':
                userData[session]['offset'] += 1
            elif button == 'Previous':
                userData[session]['offset'] -= 1

        logging.info(intent, userData[session])
        if intent == 'Default Welcome Intent':
            return welcomeCard(), 200
        elif intent == 'launches':
            return launchCard(data, userData, session), 200
        return json.dumps({'fulfillmentText' : 'test'}), 200
    else:
        abort(400)

def welcomeCard():
    response = {}
    with open(os.path.join('cards','home.json')) as f:
        response = json.loads(f.read())
    return response

def launchCard(data, userData, session):
    response = {}

    # Offset
    offset = userData[session]['offset']
    launch = data[offset]
    # Countdown
    net = launch['net']
    launchTime = parse(net)
    currentTime = datetime.datetime.now(datetime.timezone.utc)
    
    difference = launchTime-currentTime
    days, hours, minutes, seconds = timeDeltaExpand(difference)
    countdownString = formatCountdown(days, hours, minutes, seconds)

    # Vehicle, mission info
    launchVehicle = launch['rocket']['name']
    mission = ''
    missionDescription = ''
    if len(launch['missions']) > 0:
        mission = launch['missions'][0]
        missionDescription = mission['description']
    missionName = launch['name'][len(launchVehicle)+3:]

    responseItems = [
        {
            'simpleResponse': {
                'textToSpeech': 'Here\'s what I found on the {} upcoming launch:'.format(formatNumberSuffix(offset+1))
            }
        },
        {
            'basicCard': {
                'title': '{} | {}'.format(missionName, launchVehicle),
                'subtitle': 'Launches in {}'.format(countdownString),
                'formattedText': 'Scheduled for {}'.format(net),
                'image': {
                    'url': launch['rocket']['imageURL'],
                    'accessibilityText': launchVehicle
                }
            }
        }
    ]

    # Suggestion chips
    suggestions = [
        {'title': 'Next'},
        # {'title': 'More Info'},
        {'title': 'Home'}
    ]
    if offset > 0:
        suggestions.insert(0, {'title': 'Previous'})

    # Adding mission description
    if mission != '':
        responseItems[1]['basicCard']['formattedText'] += '  \n{}'.format(missionDescription)

    # responseItems[1]['basicCard']['formattedText'] += '  \nLaunching from: {}'.format(launch['location']['pads'][0]['name'])

    # Livestream link
    if len(launch['vidURLs']) > 0:
        responseItems[1]['basicCard']['buttons'] = [{
            'title': 'Watch Live',
            'openUrlAction': {
                'url': launch['vidURLs'][0]
            }
        }]

    response['payload']['google']['richResponse']['items'] = responseItems

    response['payload']['google']['richResponse']['suggestions'] = suggestions

    return response

# reloadData(data)

if __name__ == '__main__':
    app.run(debug=False,host='0.0.0.0',port=int(os.environ.get('PORT', 8080)))