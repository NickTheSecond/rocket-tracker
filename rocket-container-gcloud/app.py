import json, os, sys, time, codecs, logging
import requests
import datetime
import dateutil
from dateutil.parser import parse
from flask import Flask, request, abort, render_template
from utils import *
import threading

# Google cloud stuff
from google.cloud import datastore
datastore_client = datastore.Client()


os.chdir(os.path.dirname(os.path.abspath(__file__)))

app = Flask(__name__)
prefixes = ['agency','agencytype','launch','launchstatus','location','mission','missiontype','pad','payload','rocket','rocketfamily']
logging.basicConfig(level=logging.INFO)
data = {}
userData = {}

loadData('data', data)


@app.route('/', methods=['POST'])
def webhook():
    global userData
    global data
    if request.method == 'POST':
        # print(request.json)
        requestData = request.json
        intent = requestData['queryResult']['intent']['displayName']
        session = requestData['session']
        
        if session not in userData:
            userData[session] = {'offset' : 0}
        
        # Handling button press
        if 'button' in requestData['queryResult']['parameters']:
            button = requestData['queryResult']['parameters']['button']
            if button == 'Next':
                userData[session]['offset'] += 1
            elif button == 'Previous':
                userData[session]['offset'] -= 1

        logging.info(intent, userData[session])
        if intent == 'Default Welcome Intent':
            return welcomeCard(), 200
        elif intent == 'launches':
            return launchCard(data, userData, session), 200
        return json.dumps({'fulfillmentText' : 'test'}), 200
    else:
        abort(400)

def welcomeCard():
    response = {}
    with open(os.path.join('cards','home.json')) as f:
        response = json.loads(f.read())
    return response

def launchCard(data, userData, session):
    response = {}
    with open(os.path.join('cards','launch.json')) as f:
        response = json.loads(f.read())
    
    launch, net, launchTime, currentTime = [None for i in range(4)]
    # Offset
    offset = userData[session]['offset']
    i = 0 # Invisible offset
    while True:
        launch = data['launch']['launches'][offset+i]
        # Countdown
        net = launch['net']
        launchTime = parse(net)
        currentTime = datetime.datetime.now(datetime.timezone.utc)
        if launchTime > currentTime:
            break
        i += 1
    difference = launchTime-currentTime
    days, hours, minutes, seconds = timeDeltaExpand(difference)
    countdownString = formatCountdown(days, hours, minutes, seconds)

    # Vehicle, mission info
    launchVehicle = launch['rocket']['name']
    mission = ''
    missionDescription = ''
    if len(launch['missions']) > 0:
        mission = launch['missions'][0]
        missionDescription = mission['description']
    missionName = launch['name'][len(launchVehicle)+3:]

    responseItems = [
        {
            'simpleResponse': {
                'textToSpeech': 'Here\'s what I found on the {} upcoming launch:'.format(formatNumberSuffix(offset+1))
            }
        },
        {
            'basicCard': {
                'title': '{} | {}'.format(missionName, launchVehicle),
                'subtitle': 'Launches in {}'.format(countdownString),
                'formattedText': 'Scheduled for {}'.format(net),
                'image': {
                    'url': launch['rocket']['imageURL'],
                    'accessibilityText': launchVehicle
                }
            }
        }
    ]

    # Suggestion chips
    suggestions = [
        {'title': 'Next'},
        # {'title': 'More Info'},
        {'title': 'Home'}
    ]
    if offset > 0:
        suggestions.insert(0, {'title': 'Previous'})

    # Adding mission description
    if mission != '':
        responseItems[1]['basicCard']['formattedText'] += '  \n{}'.format(missionDescription)

    # responseItems[1]['basicCard']['formattedText'] += '  \nLaunching from: {}'.format(launch['location']['pads'][0]['name'])

    # Livestream link
    if len(launch['vidURLs']) > 0:
        responseItems[1]['basicCard']['buttons'] = [{
            'title': 'Watch Live',
            'openUrlAction': {
                'url': launch['vidURLs'][0]
            }
        }]

    response['payload']['google']['richResponse']['items'] = responseItems

    response['payload']['google']['richResponse']['suggestions'] = suggestions

    return response

reloadData(data)

if __name__ == '__main__':
    app.run(debug=False,host='0.0.0.0',port=int(os.environ.get('PORT', 8080)))