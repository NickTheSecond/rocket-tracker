import json, os, sys, time, codecs, logging
import requests
import datetime
import dateutil
from dateutil.parser import parse
from flask import Flask, request, abort
from utils import *

os.chdir(os.path.dirname(os.path.abspath(__file__)))
app = Flask(__name__)
logging.basicConfig(level=logging.INFO)
userData = {}

@app.route('/', methods=['POST'])
def webhook():
    global userData
    if request.method == 'POST':
        # print(request.json)
        requestData = request.json
        intent = requestData['queryResult']['intent']['displayName']
        session = requestData['session']
        
        if session not in userData:
            userData[session] = {'offset' : 0, 'erroroffset' : 0}
        
        # Handling button press
        if 'button' in requestData['queryResult']['parameters']:
            button = requestData['queryResult']['parameters']['button']
            if button == 'Next':
                userData[session]['offset'] += 1
            elif button == 'Previous':
                userData[session]['offset'] -= 1

        logging.info(intent, userData[session])
        if intent == 'Default Welcome Intent':
            userData[session]['offset'] = 0
            userData[session]['erroroffset'] = 0
            return welcomeCard(), 200
        elif 'launches' in intent:
            return launchCard(userData, session), 200
        return json.dumps({'fulfillmentText' : 'No Intent Found'}), 200
    else:
        abort(400)

def welcomeCard():
    response = {}
    with open(os.path.join('cards','home.json')) as f:
        response = json.loads(f.read())
    return response

def launchCard(userData, session):
    response = {}
    offset = userData[session]['offset']
    url = 'https://launchlibrary.net/1.4/launch?mode=verbose&next=3&offset={}'.format(offset)
    r = requests.get(url = url) 

    with open(os.path.join('cards','launch.json')) as f:
        response = json.loads(f.read())
    
    

    # Countdown
    launch, net, launchTime, currentTime, difference, days, hours, minutes, seconds ,countdownString = [None for i in range(10)]
    for i in range(3):
        launch = r.json()['launches'][i]
        net = launch['net']
        launchTime = parse(net)
        currentTime = datetime.datetime.now(datetime.timezone.utc)
        difference = launchTime-currentTime
        days, hours, minutes, seconds = timeDeltaExpand(difference)
        countdownString = formatCountdown(days, hours, minutes, seconds)
        if days >= 0:
            break
        else:
            userData[session]['offset'] += 1
            userData[session]['erroroffset'] += 1

    # Vehicle, mission info
    launchVehicle = launch['rocket']['name']
    mission = ''
    missionDescription = ''
    if len(launch['missions']) > 0:
        mission = launch['missions'][0]
        missionDescription = mission['description']
    missionName = launch['name'][len(launchVehicle)+3:]

    responseItems = [
        {
            'simpleResponse': {
                'textToSpeech': 'Here\'s what I found on the {} upcoming launch:'.format(formatNumberSuffix(userData[session]['offset']-userData[session]['erroroffset']+1))
            }
        },
        {
            'basicCard': {
                'title': '{} | {}'.format(missionName, launchVehicle),
                'subtitle': 'Launches in {}'.format(countdownString),
                'formattedText': 'Scheduled for {}'.format(net),
                'image': {
                    'url': launch['rocket']['imageURL'],
                    'accessibilityText': launchVehicle
                }
            }
        }
    ]

    # Suggestion chips
    suggestions = [
        {'title': 'Next'},
        # {'title': 'More Info'},
        {'title': 'Home'}
    ]
    if offset > 0:
        suggestions.insert(0, {'title': 'Previous'})

    # Adding mission description
    if mission != '':
        responseItems[1]['basicCard']['formattedText'] += '  \n{}'.format(missionDescription)

    # responseItems[1]['basicCard']['formattedText'] += '  \nLaunching from: {}'.format(launch['location']['pads'][0]['name'])

    # Livestream link
    if len(launch['vidURLs']) > 0:
        responseItems[1]['basicCard']['buttons'] = [{
            'title': 'Watch Live',
            'openUrlAction': {
                'url': launch['vidURLs'][0]
            }
        }]

    response['payload']['google']['richResponse']['items'] = responseItems

    response['payload']['google']['richResponse']['suggestions'] = suggestions

    logging.info(response)
    return response

if __name__ == '__main__':
    app.run(debug=False,host='0.0.0.0',port=int(os.environ.get('PORT', 8080)))